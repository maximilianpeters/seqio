/*
 *  Permission to use, copy, modify, distribute, and sell this software
 *  for any purpose and without fee, restriction or acknowledgement is
 *  hereby granted.  The author (James Knight of the Univ. of California,
 *  Davis) places it in the public domain.
 *
 *  This software is provided AS IS with no warranties of any kind.  The
 *  author shall have no liability with respect to the infringement of
 *  copyrights, trade secrets or any patents by this software or any part
 *  thereof.  In no event will the author be liable for any lost revenue
 *  or profits or other special, indirect and consequential damages.
 */

#include <stdio.h>
#include <stdlib.h>
#include "seqio.h"

/*
 * Think of this program as a combination of a `cat' program for
 * biological sequences and an `extract' program for extracting
 * entries from biological databases.  Since the SEQIO package
 * has the ability to transparently (as far as the program is
 * concerned) read either complete files and databases or just
 * single entries of files and databases, this simple program
 * performs both of those functions.
 */

int main(int argc, char *argv[])
{
  int i, len;
  char *entry;
  SEQFILE *sfp;

  if (argc == 1) {
    if ((sfp = seqfopen2("-")) != NULL) {
      while ((entry = seqfgetentry(sfp, &len, 0)) != NULL)
        fwrite(entry, len, 1, stdout);

      seqfclose(sfp);
    }
  }
  else {
    for (i=1; i < argc; i++) {
      if ((sfp = seqfopen2(argv[i])) == NULL)
        continue;

      while ((entry = seqfgetentry(sfp, &len, 0)) != NULL)
        fwrite(entry, len, 1, stdout);

      seqfclose(sfp);
    }
  }

  return 0;
}
