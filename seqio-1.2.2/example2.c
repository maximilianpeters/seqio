/*
 *  Permission to use, copy, modify, distribute, and sell this software
 *  for any purpose and without fee, restriction or acknowledgement is
 *  hereby granted.  The author (James Knight of the Univ. of California,
 *  Davis) places it in the public domain.
 *
 *  This software is provided AS IS with no warranties of any kind.  The
 *  author shall have no liability with respect to the infringement of
 *  copyrights, trade secrets or any patents by this software or any part
 *  thereof.  In no event will the author be liable for any lost revenue
 *  or profits or other special, indirect and consequential damages.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "seqio.h"

/*
 * This program simply outputs all of the information that
 * the SEQIO package can automaticallly extract from an entry.
 */

int main(int argc, char *argv[])
{
  int i;
  SEQFILE *sfp;
  SEQINFO *info;

  i = 1;
  for ( ; i < argc; i++) {
    if ((sfp = seqfopen2(argv[i])) == NULL)
      continue;

    while ((info = seqfgetinfo(sfp, 0)) != NULL) {
      printf("\n");
      printf("Sequence %d of %d, in entry %d of file %s\n", info->seqno,
             info->numseqs, info->entryno, info->filename);
      printf("Database:  `%s'\n", info->dbname);
      printf("Format:  `%s'\n", info->format);

      printf("Identifiers:  `%s'\n", info->idlist);
      printf("Properties:  %s, %s, %s of length %d (rawlen %d)\n", 
             (info->iscircular ? "Circular" : "Linear"),
             (info->alphabet == DNA ? "DNA"
                : (info->alphabet == RNA ? "RNA"
                     : (info->alphabet == PROTEIN ? "Protein" : "Unknown"))),
             (info->isfragment ? "Fragment" : "Complete Sequence"),
             info->truelen, info->rawlen);

      printf("Date:  `%s'\n", info->date);
      printf("Organism:  `%s'\n", info->organism);
      printf("Description:  `%s'\n", info->description);
      printf("Comment:  `\n%s'\n", info->comment);
      printf("History:  `\n%s'\n", info->history);
      printf("\n");
    }

    seqfclose(sfp);
  }

  return 0;
}
