
---

## SEQIO: A Package for Reading and Writing Sequence Files

SeqIO is an abandoned package for converting sequence files.
Full description: SEQIO: A Package for Reading and Writing Sequence Files
Downloaded from here: http://web.cs.ucdavis.edu/~gusfield/seqio.html


---

## Installation

cd seqio-1.2.2
make
